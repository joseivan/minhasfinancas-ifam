package br.edu.ifam.joseivanandroid.minhasfinanas.data;

import java.util.Date;

import io.realm.RealmObject;

public class FinancialEntry extends RealmObject {

    public static final int TYPE_INCOME = 1;
    public static final int TYPE_EXPENSE = 2;

    private int mId;
    private int mType;
    private float mValue;
    private long mTime;
    private String mTag;
    private String mDescription;

    public FinancialEntry() {
    }

    public FinancialEntry(float value, Date date, String description, String tag) {
        mType = (value >= 0 ? TYPE_INCOME : TYPE_EXPENSE);
        mValue = value;
        mTag = tag;
        mTime = date.getTime();
        mDescription = description;
    }

    public FinancialEntry(float value, Date date) {
        this(value, date, null, null);
    }

    public int getId() {
        return mId;
    }

    public void setId(int id) {
        mId = id;
    }

    public int getType() {
        return mType;
    }

    public float getValue() {
        return mValue;
    }

    public String getTag() {
        return mTag;
    }

    public long getTime() {
        return mTime;
    }

    public Date getDate() {
        return new Date(mTime);
    }

    public String getDescription() {
        return mDescription;
    }

    public void setType(int mType) {
        this.mType = mType;
    }

    public void setValue(float mValue) {
        this.mValue = mValue;
    }

    public void setTime(Date date) {
        this.mTime = date.getTime();
    }

    public void setTag(String mTag) {
        this.mTag = mTag;
    }

    public void setDescription(String mDescription) {
        this.mDescription = mDescription;
    }
}
