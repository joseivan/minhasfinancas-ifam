package br.edu.ifam.joseivanandroid.minhasfinanas.ui.adapter;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;

import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.MonthEntryPage;

import static br.edu.ifam.joseivanandroid.minhasfinanas.Constants.MONTH_YEAR_FORMATTER;

public class FinancialEntryPagerAdapter extends PagerAdapter {

    private MonthEntryPage[] mMonthPages = new MonthEntryPage[12];
    private int mYear;

    public FinancialEntryPagerAdapter(int year, int type) {
        super();
        mYear = year;
        for (int i = 0; i < mMonthPages.length; ++i) {
            mMonthPages[i] = new MonthEntryPage(i + 1, year, type);
        }
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        View v = mMonthPages[position].createView(container);
        container.addView(v);

        return v;
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.MONTH, position);
        calendar.set(Calendar.YEAR, mYear);

        return MONTH_YEAR_FORMATTER.format(calendar.getTime());
    }

    @Override
    public int getCount() {
        return mMonthPages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object o) {
        return view == o;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

    public void reloadItem(int pos) {
        mMonthPages[pos].reloadData();
    }
}
