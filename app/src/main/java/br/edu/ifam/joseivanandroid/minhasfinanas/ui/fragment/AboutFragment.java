package br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;

public class AboutFragment extends FragmentBase {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    @Override
    public String getTitle() {
        return getString(R.string.action_about);
    }
}
