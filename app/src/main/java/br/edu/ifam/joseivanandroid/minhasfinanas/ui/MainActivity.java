package br.edu.ifam.joseivanandroid.minhasfinanas.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment.AboutFragment;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment.FinancialEntryFragment;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment.FragmentBase;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    private static final int INCOME_FRAGMENT = 1;
    private static final int EXPENSE_FRAGMENT = 2;
    private static final int ABOUT_FRAGMENT = 3;

    @BindView(R.id.drawer)
    DrawerLayout mSideBar;
    @BindView(R.id.toolbar)
    Toolbar mToolbar;
    @BindView(R.id.navigation)
    NavigationView mNavigationView;

    private Unbinder mUnbinder;

    private ActionBarDrawerToggle mBarToogler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_drawer);
        mUnbinder = ButterKnife.bind(this);

        setSupportActionBar(mToolbar);

        mBarToogler = new ActionBarDrawerToggle(this, mSideBar,  R.string.drawer_open,
                R.string.drawer_close);
        mSideBar.addDrawerListener(mBarToogler);
        mBarToogler.syncState();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNavigationView.setNavigationItemSelectedListener(this::handleNavigationItem);

        FragmentBase fragment = getFragment(INCOME_FRAGMENT);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.content_main, fragment)
                .commitNow();
        updateToolbarTitle(fragment.getTitle());
    }

    @Override
    protected void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    private boolean handleNavigationItem(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_income:
                showFragment(getFragment(INCOME_FRAGMENT));
                break;
            case R.id.action_expenses:
                showFragment(getFragment(EXPENSE_FRAGMENT));
                break;
            case R.id.action_about:
                showFragment(getFragment(ABOUT_FRAGMENT));
                break;
            case R.id.action_charts:
                startActivity(new Intent(this, ChartActivity.class));
                break;
        }

        mSideBar.closeDrawers();
        return true;
    }

    private FragmentBase getFragment(int id) {
        switch (id) {
            case INCOME_FRAGMENT: {
                FragmentBase f = new FinancialEntryFragment();
                Bundle args = new Bundle();
                args.putInt(FinancialEntryFragment.EXTRAS_TYPE, FinancialEntry.TYPE_INCOME);
                f.setArguments(args);
                return f;
            }
            case EXPENSE_FRAGMENT: {
                FragmentBase f = new FinancialEntryFragment();
                Bundle args = new Bundle();
                args.putInt(FinancialEntryFragment.EXTRAS_TYPE, FinancialEntry.TYPE_EXPENSE);
                f.setArguments(args);
                return f;
            }
            case ABOUT_FRAGMENT:
                return new AboutFragment();
        }

        return null;
    }

    private void updateToolbarTitle(String str) {
        getSupportActionBar().setTitle(str);
    }

    private void showFragment(FragmentBase fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.content_main, fragment, "Main")
                .commitNow();
        updateToolbarTitle(fragment.getTitle());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mBarToogler.onOptionsItemSelected(item)) {
            return true;
        }
        int id = item.getItemId();

        return super.onOptionsItemSelected(item);
    }
}
