package br.edu.ifam.joseivanandroid.minhasfinanas.data;

import android.util.Log;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import io.realm.Realm;
import io.realm.RealmResults;

import static br.edu.ifam.joseivanandroid.minhasfinanas.Constants.LOG_TAG;

public class DatabaseManager {

    public static void deleteEntry(FinancialEntry entry) {
        Realm r = Realm.getDefaultInstance();
        r.beginTransaction();
        r.where(FinancialEntry.class).equalTo("mId", entry.getId()).findAll().deleteAllFromRealm();
        r.commitTransaction();
    }

    public static void addFinancialEntry(FinancialEntry entry) {
        Log.d(LOG_TAG, String.format("New entry time: %d", entry.getTime()));
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        Number cId = realm.where(FinancialEntry.class).max("mId");
        if (cId == null) {
            entry.setId(1);
        } else {
            entry.setId(cId.intValue() + 1);
        }
        realm.copyToRealm(entry);
        realm.commitTransaction();
    }

    public static List<FinancialEntry> getIncomeEntriesByMonth(int month, int year) {
        return getEntriesByMonth(month, year, FinancialEntry.TYPE_INCOME);
    }

    public static List<FinancialEntry> getExpenseEntriesByMonth(int month, int year) {
        return getEntriesByMonth(month, year, FinancialEntry.TYPE_EXPENSE);
    }

    public static List<FinancialEntry> getEntriesByMonth(int month, int year, int type) {
        Calendar c = Calendar.getInstance();
        setInitialDate(c);
        c.set(Calendar.MONTH, month - 1);
        c.set(Calendar.YEAR, year);
        long begin = c.getTime().getTime();
        setEndDate(c);
        long end = c.getTime().getTime();

        Log.d(LOG_TAG, String.format("Search by month range (%d, %d)", begin, end));
        Realm realm = Realm.getDefaultInstance();
        RealmResults<FinancialEntry> result = realm.where(FinancialEntry.class)
                .equalTo("mType", type)
                .between("mTime", begin, end).findAll();

        return returnList(realm, result);
    }

    public static List<FinancialEntry> getAllEntriesByType(int type) {
        Realm realm = Realm.getDefaultInstance();
        RealmResults<FinancialEntry> result = realm
                .where(FinancialEntry.class).equalTo("mType", type).findAll();
        return returnList(realm, result);
    }

    public static List<FinancialEntry> returnList(Realm realm, RealmResults<FinancialEntry> result) {
        ArrayList<FinancialEntry> resList = new ArrayList<>(result.size());
        for (FinancialEntry fe : result) {
            Log.d(LOG_TAG, String.format("Entry time: %d", fe.getTime()));
            resList.add(realm.copyFromRealm(fe));
        }

        return resList;
    }


    private static void setInitialDate(Calendar c) {
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.DAY_OF_MONTH, 1);
    }

    private static void setEndDate(Calendar c) {
        c.set(Calendar.HOUR, 23);
        c.set(Calendar.MINUTE, 59);
        c.set(Calendar.SECOND, 59);
        // Verify for month with less than 31 days
        c.set(Calendar.DAY_OF_MONTH, 31);
    }
}
