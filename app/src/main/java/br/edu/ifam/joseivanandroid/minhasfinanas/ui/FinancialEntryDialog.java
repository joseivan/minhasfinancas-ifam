package br.edu.ifam.joseivanandroid.minhasfinanas.ui;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.DatePicker;
import android.widget.Toast;
import android.widget.Toolbar;

import java.util.Calendar;
import java.util.Date;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.databinding.DialogAddBinding;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static br.edu.ifam.joseivanandroid.minhasfinanas.Constants.DATE_FORMATTER;

public class FinancialEntryDialog extends Dialog {

    public interface OnEntryCreatedListener {
        void onEntryCreated(Dialog dialog, FinancialEntry entry);
    }

    private OnEntryCreatedListener mListener;
    private Unbinder mUnbinder;
    private DialogAddBinding mBinding;
    private FinancialEntry mEntry = new FinancialEntry(0, new Date());

    public FinancialEntryDialog(@NonNull Context context, int type) {
        super(context);
        mEntry.setType(type);
    }

    public FinancialEntryDialog(@NonNull Context context, int themeResId, int type) {
        super(context, themeResId);
        mEntry.setType(type);
    }

    protected FinancialEntryDialog(@NonNull Context context, boolean cancelable, @Nullable DialogInterface.OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
    }

    public void setOnEntryCreatedListener(OnEntryCreatedListener listener) {
        mListener = listener;
    }

    private void fireListener(FinancialEntry entry) {
        if (mListener != null) {
            mListener.onEntryCreated(this, entry);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mBinding = DataBindingUtil.inflate(LayoutInflater.from(getContext()), R.layout.dialog_add,
                null, false);
        setContentView(mBinding.getRoot());
        mUnbinder = ButterKnife.bind(this);

        Calendar c = Calendar.getInstance();
        mEntry.setTime(c.getTime());
        mBinding.setEntry(mEntry);
        mBinding.setDialog(this);
        switch (mEntry.getType()) {
            case FinancialEntry.TYPE_INCOME:
                mBinding.spinnerEntryType.setSelection(0);
                break;
            case FinancialEntry.TYPE_EXPENSE:
                mBinding.spinnerEntryType.setSelection(1);
                break;
        }

        updateDateString();
    }

    @Override
    protected void onStop() {
        mUnbinder.unbind();
        super.onStop();
    }

    @OnClick(R.id.button_date)
    public void onDateButton() {
        Calendar c = Calendar.getInstance();
        c.setTime(mEntry.getDate());
        DatePickerDialog datePicker = new DatePickerDialog(getContext(), new DatePickerListener(),
                c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        datePicker.show();
    }

    @OnClick(R.id.button_positive)
    public void onPositiveButton() {
        if (mEntry.getDescription() == null || mEntry.getDescription().isEmpty()
                || mBinding.editValue.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), getContext().getString(R.string.err_fill_entry),
                    Toast.LENGTH_SHORT).show();
            return;
        }

        fireListener(mEntry);
    }

    @OnClick(R.id.button_negative)
    public void onNegativeButton() {
        dismiss();
    }

    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        String selected = (String) parent.getItemAtPosition(position);
        if (selected.equals(getContext().getString(R.string.entry_income))) {
            mEntry.setType(FinancialEntry.TYPE_INCOME);
        } else if (selected.equals(getContext().getString(R.string.entry_expense))) {
            mEntry.setType(FinancialEntry.TYPE_EXPENSE);
        }
    }

    private void updateDateString() {
        mBinding.tvDate.setText(DATE_FORMATTER.format(mEntry.getDate()));
    }

    private class DatePickerListener implements DatePickerDialog.OnDateSetListener {

        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            Calendar c = Calendar.getInstance();
            c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            c.set(Calendar.MONTH, month);
            c.set(Calendar.YEAR, year);
            mEntry.setTime(c.getTime());
            updateDateString();
        }
    }
}
