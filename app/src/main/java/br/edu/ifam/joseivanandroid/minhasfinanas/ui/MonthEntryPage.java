package br.edu.ifam.joseivanandroid.minhasfinanas.ui;

import android.graphics.Color;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.adapter.FinancialEntryAdapter;

public class MonthEntryPage {

    private int mMonth;
    private int mYear;
    private int mType;
    private View v;
    private FinancialEntryAdapter mAdapter;

    public MonthEntryPage(int month, int year, int type) {
        mMonth = month;
        mYear = year;
        mType = type;
        mAdapter = new FinancialEntryAdapter(mMonth, mYear, mType);
    }

    private void updateTotalAmount() {
        ((TextView) v.findViewById(R.id.tv_total_amount))
                .setText(String.format("R$ %.2f", mAdapter.getAmountTotal()));
    }

    public View createView(ViewGroup parent) {
        v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_entry, parent, false);
        LinearLayoutManager manager = new LinearLayoutManager(parent.getContext());
        RecyclerView rv = v.findViewById(R.id.list);
        rv.setLayoutManager(manager);
        rv.setHasFixedSize(true);
        rv.setAdapter(mAdapter);

        if (mType == FinancialEntry.TYPE_INCOME) {
            ((TextView) v.findViewById(R.id.tv_total_amount)).setTextColor(Color.BLUE);
        } else {
            ((TextView) v.findViewById(R.id.tv_total_amount)).setTextColor(Color.RED);
        }

        mAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                v.findViewById(R.id.empty)
                        .setVisibility(mAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
                updateTotalAmount();
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount) {
            }

            @Override
            public void onItemRangeChanged(int positionStart, int itemCount, @Nullable Object payload) {
            }

            @Override
            public void onItemRangeInserted(int positionStart, int itemCount) {
                v.findViewById(R.id.empty).setVisibility(View.GONE);
                updateTotalAmount();
            }

            @Override
            public void onItemRangeRemoved(int positionStart, int itemCount) {
                v.findViewById(R.id.empty)
                        .setVisibility(mAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
                updateTotalAmount();
            }

            @Override
            public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            }
        });
        v.findViewById(R.id.empty)
                .setVisibility(mAdapter.getItemCount() > 0 ? View.GONE : View.VISIBLE);
        updateTotalAmount();

        return v;
    }

    public View getView() {
        return v;
    }

    public void reloadData() {
        mAdapter.reloadData();
    }
}
