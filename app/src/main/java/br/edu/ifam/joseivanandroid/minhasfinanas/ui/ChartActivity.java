package br.edu.ifam.joseivanandroid.minhasfinanas.ui;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.helper.DateAsXAxisLabelFormatter;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.DatabaseManager;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment.FragmentBase;

public class ChartActivity extends AppCompatActivity {

    private GraphView mChart;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_chart);
        mChart = findViewById(R.id.graph);
        setupChart();
    }

    private void setupChart() {

        List<FinancialEntry> dataIncome = DatabaseManager.getAllEntriesByType(FinancialEntry.TYPE_INCOME);
        List<FinancialEntry> dataExpense = DatabaseManager.getAllEntriesByType(FinancialEntry.TYPE_EXPENSE);

        LineGraphSeries<DataPoint> seriesIncome = new LineGraphSeries<>(aggregateData(dataIncome));
        LineGraphSeries<DataPoint> seriesExpense = new LineGraphSeries<>(aggregateData(dataExpense));

        seriesExpense.setColor(Color.RED);
        seriesIncome.setColor(Color.BLUE);

        mChart.addSeries(seriesIncome);
        mChart.addSeries(seriesExpense);

        mChart.getGridLabelRenderer().setLabelFormatter(new DateAsXAxisLabelFormatter(this) {
            private SimpleDateFormat mFormatter = new SimpleDateFormat("MM/yyyy");

            @Override
            public String formatLabel(double value, boolean isValueX) {
                if (isValueX) {
                    mCalendar.setTimeInMillis((long) value);
                    return mFormatter.format(mCalendar.getTime());
                }

                return super.formatLabel(value, isValueX);
            }
        });
        mChart.getViewport().setScalable(true);
        mChart.getViewport().setScalableY(true);
        mChart.getViewport().setScrollable(true);
        mChart.getViewport().setScrollableY(true);
    }

    private DataPoint[] aggregateData(List<FinancialEntry> data) {
        LinkedList<DataPoint> aggregatedata = new LinkedList<DataPoint>();
        HashMap<Integer, int[]> mapData = new HashMap<>();
        Calendar c = Calendar.getInstance();
        for (FinancialEntry fe : data) {
            c.setTime(fe.getDate());
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);

            int[] yeardata = mapData.get(year);
            if (yeardata == null) {
                yeardata = new int[12];
                mapData.put(year, yeardata);
            }

            yeardata[month] += fe.getValue();
        }

        c.set(Calendar.DAY_OF_MONTH, 1);
        c.set(Calendar.HOUR, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        for (int year : mapData.keySet()) {
            int[] monthsData = mapData.get(year);
            for (int i = 0; i < 12; ++i) {
                c.set(Calendar.YEAR, year);
                c.set(Calendar.MONTH, i);
                aggregatedata.add(new DataPoint(c.getTime(), monthsData[i]));
            }
        }

        return aggregatedata.toArray(new DataPoint[0]);
    }
}
