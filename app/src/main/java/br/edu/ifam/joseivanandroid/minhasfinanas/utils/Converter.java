package br.edu.ifam.joseivanandroid.minhasfinanas.utils;

import android.databinding.InverseMethod;

import java.util.Date;

import static br.edu.ifam.joseivanandroid.minhasfinanas.Constants.DATE_FORMATTER;

public class Converter {

    @InverseMethod("floatToString")
    public static float stringToFloat(String value) {
        if (value == null || value.isEmpty()) {
            return 0.0f;
        }

        return Float.parseFloat(value);
    }

    public static String floatToString(float value) {
        return String.valueOf(value);
    }

    public static String dateToString(Date date) {
        if (date == null) {
            return "";
        }

        return DATE_FORMATTER.format(date);
    }

    public static String valueToString(float value) {
        return String.format("R$ %.2f", value);
    }
}
