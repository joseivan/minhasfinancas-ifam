package br.edu.ifam.joseivanandroid.minhasfinanas.ui.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import java.util.List;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.DatabaseManager;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.view.FinancialEntryViewHolder;

public class FinancialEntryAdapter extends RecyclerView.Adapter<FinancialEntryViewHolder> {

    private List<FinancialEntry> mData;
    private int mMonth;
    private int mYear;
    private int mType;
    private double mAmount = 0;

    public FinancialEntryAdapter(int month, int year, int type) {
        mMonth = month;
        mYear = year;
        mType = type;
        mData = DatabaseManager.getEntriesByMonth(month, year, type);
        reloadAmount();
    }

    private void reloadAmount() {
        double newAmount = 0;
        for (FinancialEntry fe : mData) {
            newAmount += fe.getValue();
        }

        mAmount = newAmount;
    }

    public void reloadData() {
        mData = DatabaseManager.getEntriesByMonth(mMonth, mYear, mType);
        reloadAmount();
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public FinancialEntryViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        return new FinancialEntryViewHolder(LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.item_financial_entry, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull FinancialEntryViewHolder financialEntryViewHolder, int i) {
        financialEntryViewHolder.setFinancialEntry(mData.get(i));
        financialEntryViewHolder.getDeleteButton().setOnClickListener((v) -> {
            int pos = financialEntryViewHolder.getAdapterPosition();
            if (pos < mData.size()) {
                DatabaseManager.deleteEntry(mData.get(pos));
                mData.remove(pos);
                reloadAmount();
                notifyItemRemoved(pos);
            }
        });
    }

    @Override
    public int getItemCount() {
        return mData.size();
    }

    public double getAmountTotal() {
        return mAmount;
    }
}
