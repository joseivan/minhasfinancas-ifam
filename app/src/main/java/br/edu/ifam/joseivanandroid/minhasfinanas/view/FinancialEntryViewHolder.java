package br.edu.ifam.joseivanandroid.minhasfinanas.view;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.databinding.ItemFinancialEntryBinding;

public class FinancialEntryViewHolder extends RecyclerView.ViewHolder {

    private ItemFinancialEntryBinding binding;

    public FinancialEntryViewHolder(@NonNull View itemView) {
        super(itemView);
        binding = DataBindingUtil.bind(itemView);
    }

    public void setFinancialEntry(FinancialEntry entry) {
        binding.setEntry(entry);
    }

    public View getDeleteButton() {
        return binding.deleteButton;
    }
}
