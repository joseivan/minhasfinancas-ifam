package br.edu.ifam.joseivanandroid.minhasfinanas;

import java.text.SimpleDateFormat;

public final class Constants {

    public static final String LOG_TAG = "MinhasFinancas";

    public static final SimpleDateFormat MONTH_YEAR_FORMATTER = new SimpleDateFormat("MMM/yyyy");

    public static final SimpleDateFormat DATE_FORMATTER = new SimpleDateFormat("dd/MM/yyyy");

}