package br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.Calendar;

import br.edu.ifam.joseivanandroid.minhasfinanas.R;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.DatabaseManager;
import br.edu.ifam.joseivanandroid.minhasfinanas.data.FinancialEntry;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.FinancialEntryDialog;
import br.edu.ifam.joseivanandroid.minhasfinanas.ui.adapter.FinancialEntryPagerAdapter;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

public class FinancialEntryFragment extends FragmentBase {

    public static final String EXTRAS_TYPE
            = "br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment.FinancialEntryFragment.EXTRAS_TYPE";
    public static final String EXTRAS_YEAR
            = "br.edu.ifam.joseivanandroid.minhasfinanas.ui.fragment.FinancialEntryFragment.EXTRAS_YEAR";

    @BindView(R.id.view_pager)
    ViewPager mPager;
    @BindView(R.id.tab_layout)
    TabLayout mTab;
    @BindView(R.id.fab)
    FloatingActionButton mAddAction;

    private Unbinder mUnbinder;
    private FinancialEntryPagerAdapter mPageAdapter;
    private int mType = FinancialEntry.TYPE_INCOME;
    private int mYear;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);

        Bundle args = getArguments();
        if (args != null) {
            mType = args.getInt(EXTRAS_TYPE);
            mYear = args.getInt(EXTRAS_YEAR, mYear);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.content_income, container, false);
        mUnbinder = ButterKnife.bind(this, v);
        mPageAdapter = new FinancialEntryPagerAdapter(mYear, mType);
        mPager.setAdapter(mPageAdapter);

        mAddAction.setOnClickListener((view) -> {
            FinancialEntryDialog d = new FinancialEntryDialog(getContext(), mType);
            d.setOnEntryCreatedListener((dialog, entry) -> {
                DatabaseManager.addFinancialEntry(entry);
                mPageAdapter.reloadItem(entry.getDate().getMonth());
                dialog.dismiss();
            });
            d.show();
        });

        Calendar c = Calendar.getInstance();
        mPager.setCurrentItem(c.get(Calendar.MONTH));

        return v;
    }

    @Override
    public void onDestroy() {
        mUnbinder.unbind();
        super.onDestroy();
    }

    @Override
    public String getTitle() {
        if (mType == FinancialEntry.TYPE_EXPENSE) {
            return getString(R.string.action_expenses);
        }

        return getString(R.string.action_income);
    }

    @OnClick(R.id.btn_arrow_left)
    public void onLeftClick() {
        int selected = mTab.getSelectedTabPosition();
        if (selected == 0) {
            --mYear;
            mPageAdapter = new FinancialEntryPagerAdapter(mYear, mType);
            mPager.setAdapter(mPageAdapter);
            mTab.getTabAt(11).select();
        } else {
            mTab.getTabAt(selected - 1).select();
        }
    }

    @OnClick(R.id.btn_arrow_right)
    public void onRightClick() {
        int selected = mTab.getSelectedTabPosition();
        if (selected == 11) {
            ++mYear;
            mPageAdapter = new FinancialEntryPagerAdapter(mYear, mType);
            mPager.setAdapter(mPageAdapter);
            mTab.getTabAt(0).select();
        } else {
            mTab.getTabAt(selected + 1).select();
        }
    }
}
